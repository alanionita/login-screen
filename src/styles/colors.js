export default {
  background: "#FAFAFA",
  border: "#F9F9F9",
  brandGreen: '#32af5f',
  shadow: "#F5F5F5",
  darkestGrey: "#4c494b",
  lightGrey: "#b2afb1",
  middleGrey: "#898789",
  darkGrey: "#736e72",
  blue: '#58A7DB'
};
