import { createGlobalStyle } from "styled-components";
import { normalize } from "./normalize.index";

export const GlobalStyles = createGlobalStyle`
    ${normalize}
    * {
        box-sizing: border-box;
    }
`;
