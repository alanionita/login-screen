import React from "react";
import Styled from "./index.styled";
import Typography from "../Typography/index.styled";

export default () => (
  <Styled.FormCheckbox>
    <Typography.CheckboxLabel as='p' htmlFor='remember-device'>
      Remember this device
    </Typography.CheckboxLabel>
    <Styled.FormCheckbox__input type='checkbox' required />
    <Styled.FormCheckbox__span />
  </Styled.FormCheckbox>
);
