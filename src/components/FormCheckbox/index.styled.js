import styled from "styled-components";
import colors from "../../styles/colors.js";
import breakpoints from "../../styles/breakpoints.js";

const FormCheckbox__input = styled.input`
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
`;

const FormCheckbox__span = styled.span`
  position: absolute;
  margin: auto 0;
  top: 0;
  left: 0;
  height: 26px;
  width: 26px;
  border-radius: 8px;
  border: 3px solid ${colors.border};
  margin-right: 8px;
  &:after {
    content: "";
    position: absolute;
    display: none;
  }
  @media ${breakpoints.tablet} {
    top: 20%; 
  }
`;

const FormCheckbox = styled.label`
  display: flex;
  position: relative;
  height: 26px;
  align-items: center;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  user-select: none;
  &:hover,
  &:focus {
    & ${FormCheckbox__input} ~ ${FormCheckbox__span} {
      background-color: ${colors.border};
      border: 3px solid ${colors.darkGrey};
    }
  }
  ${FormCheckbox__input}:focus ~ ${FormCheckbox__span} {
    background-color: ${colors.border};
    border: 3px solid ${colors.darkGrey};
  }
  & ${FormCheckbox__input}:checked ~ ${FormCheckbox__span} {
    background-color: ${colors.blue};
    border: none;
  }
  & ${FormCheckbox__input}:checked ~ ${FormCheckbox__span}:after {
    display: block;
  }
  & ${FormCheckbox__span}:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    transform: rotate(45deg);
  }
  @media ${breakpoints.tablet} {
    height: 42px;
  }
`;

export default {
  FormCheckbox,
  FormCheckbox__input,
  FormCheckbox__span
};
