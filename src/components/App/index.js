import React from "react";
import Styled from "./index.styled";
import Card from "../Card";

export default () => (
  <Styled.App>
    <Card />
  </Styled.App>
);
