import styled from "styled-components";

import colors from "../../styles/colors.js";
import breakpoints from "../../styles/breakpoints.js";

const App = styled.main`
  display: flex;
  flex-flow: row;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100vw;
  background-color: ${colors.background};
  @media ${breakpoints.phone} {
    height: auto;
    display: block;
  }
`;

export default {
  App
};
