import React from "react";
import Styled from "./index.styled";
import Typography from "../Typography/index.styled";
import FormCheckbox from "../FormCheckbox";

export default () => {
  const handleSubmit = event => {
    event.preventDefault();
    console.log("*************************************");
    console.log("Get back in touch with Alan, alright?");
    console.log("*************************************");
  };
  return (
    <Styled.Form onSubmit={handleSubmit.bind(this)}>
      <Typography.Label htmlFor='email'>Email address</Typography.Label>
      <Styled.Form__input name='email' type='email' required />
      <Styled.Form__CheckboxContainer>
        <FormCheckbox />
      </Styled.Form__CheckboxContainer>
      <Styled.Form__Button>Sign In</Styled.Form__Button>
    </Styled.Form>
  );
};
