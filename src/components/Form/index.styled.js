import styled from "styled-components";
import colors from "../../styles/colors.js";
import breakpoints from "../../styles/breakpoints.js";

const Form = styled.form`
  display: flex;
  flex-flow: column;
  margin-top: 72px;
  @media ${breakpoints.phone} {
    margin-top: 56px;
  }
`;

const Form__input = styled.input`
  height: 64px;
  width: 100%;
  border-radius: 8px;
  border: 3px solid ${colors.border};
  margin: 8px 0px;
  padding-left: 16px;
  padding-right: 16px;
  font-size: 20px;
  font-weight: 600;
  color: ${colors.darkGrey};
  outline: 0;
  &:focus,
  &:hover {
    background-color: ${colors.border};
    border: 3px solid ${colors.darkGrey};
  }
  @media ${breakpoints.phone} {
    height: 56px;
  }
`;

const Form__CheckboxContainer = styled.section`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  height: 64px;
`;

const Form__Button = styled.button`
  padding: 0;
  margin: 0;
  height: 64px;
  width: 172px;
  box-shadow: none;
  background-color: transparent;
  border: 3px solid ${colors.border};
  border-radius: 8px;
  color: black;
  font-size: 24px;
  margin-top: 40px;
  cursor: pointer;
  
  &:focus,
  &:hover {
    border: 3px solid ${colors.darkGrey};
    outline: none;
  }
  @media ${breakpoints.phone} {
    margin-top: 16px;
    height: 56px;
  }
`;

export default {
  Form,
  Form__input,
  Form__CheckboxContainer,
  Form__Button
};
