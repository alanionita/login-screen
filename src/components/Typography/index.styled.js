import styled from "styled-components";
import colors from "../../styles/colors.js";

const Title = styled.h1`
  color: ${colors.darkestGrey};
  font-size: 40px;
  font-weight: 600;
  padding: 0;
  margin: 0;
`;

const Subtitle = styled.h2`
  color: ${colors.lightGrey};
  font-size: 24px;
  font-weight: 600;
`;

const Label = styled.label`
  color: ${colors.middleGrey};
  font-size: 16px;
  font-weight: 600;
`;

const CheckboxLabel = styled.label`
  color: ${colors.darkGrey};
  font-size: 16px;
  font-weight: 600;
`;

export default {
  Title,
  Subtitle,
  Label,
  CheckboxLabel
};
