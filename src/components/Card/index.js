import React from "react";
import Styled from "./index.styled";
import Typography from "../Typography/index.styled";
import Logo from "../svgs/Logo";
import Form from "../Form";

export default () => (
  <Styled.Card>
    <Styled.LogoContainer>
      <Logo />
    </Styled.LogoContainer>
    <Typography.Title>Example login screen</Typography.Title>
    <Styled.Subtitle>Getting started with Green.</Styled.Subtitle>
    <Form />
  </Styled.Card>
);
