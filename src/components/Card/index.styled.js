import styled from "styled-components";
import colors from "../../styles/colors";
import breakpoints from "../../styles/breakpoints";
import Typography from "../Typography/index.styled";

const Card = styled.section`
  height: auto;
  max-width: 700px;
  background-color: white;
  box-shadow: 0 2px 3px 2px ${colors.shadow};
  margin: 0 auto;
  padding: 56px;
  @media ${breakpoints.phone} {
    padding: 28px;
  }
`;

const LogoContainer = styled.section`
  height: 45px;
  width: auto;
  margin-bottom: 40px;
  & > svg {
    height: 100%;
  }
`;

const Subtitle = styled(Typography.Subtitle)`
  margin-top: 8px;
`;

export default {
  Card,
  Subtitle,
  LogoContainer
};
