# Login Form

![](https://media.giphy.com/media/l4ogKLlyGnOzUT86vG/giphy.gif)

A simple login form built with React and styled-components (Sass) from the design provided.

[Live link](https://alanionita.gitlab.io/login-screen/)

## User stories 

Audience: energy customer looking to check their recent energy bill.

```
As a customer I can login to my account using my email (maybe a password on a second step)
```

```
As a customer I can be notified which form fields are requiered
```

```
As a customer with accesibility needs I can [TAB] to navigate through the inputs
```

```
As a customer with motor accesibility needs I can on labels to toggle checkboxes
```

```
As a customer with visual accesibility needs I can clearly read the text and I'm presented with a good selection of color contrast
```

## Rationale

### Design 

The original design was loaded in Figma and each component was sliced as required. 

I wanted to maintain visual consistency so I picked an 8pt grid and rounded each dimension to match that.

Figma is great for slicing up components, cataloging UIs, and easily tell dimensions. 

Also who has time for measuring things using the screenshot tool on Mac. 

[Live Figma Board](https://www.figma.com/file/UY0Sv9gJr1ZNDx0xffB5N4/Green-Energy)

### Frontend

React installed via Create-React-App.

I picked styles-components for styling because I realised that Green Energy also has some React Native products. Styled-components use Sass anyway an my experience is in using Sass and BEM so the styles here share some of that knowledge.

I decided early on to move components into /components, thinking that eventually we'd move to an atomic design system structure. 

Added a bit of validation to the from in preparation for a more indepth validation pass. 

From a mobile perspective the layout was fairly simple so instead building mobile-first I started at desktop size and backwards patched the mobile styles.

## Challenges and Lessons learned

- The checkbox was definitly the sneaky challenge here, but I solved it using a span hack and sibling selectors. Heavily inspired by a previous component and W3Schools
- No clear brief on how the filters should work so I implemented a solution that allows users to cycle through the filters and when you click again on a filter you return to the previous list. The clarity of this feature was hinging on correct hover and toggle styles so I reuse the existing design token and create my own.
- Borders, colors, shadows and everything design-wise was tricky. I couldn't tell what grid was used so I picked an 8 point grid with small deviations. Particularly awkward was getting the colors since the image was high-res enough to show a non-pixelated stroke. Figma to the rescue though. Challenges like this reminds me how spoilt I am to have a designers that send me designs in a spec tool like XD, Zeplin, Abstract, or Figma.
- Downloading Gotham was a bit tricky so instead I inspected green.energy and picked up the CDNed styles from there.

## Future improvements 

- A bit more animation, in particular with the logo. I felt like I could do a funky fill there to be in line with the splash of color in the branding, but ran outta time. 

## Installation
-------------

With [npm](https://npmjs.org/) installed, run

```
$ npm i
```

## Developing
-------------

```
npm run start
```


### Building

```
npm run build
```

## Acknowledgments
-------------

The login-form was inspired by.... [`green.energy website`](https://green.energy/) and the spec provided.

## See Also
-------------

- [`create-react-app`](https://reactjs.org/docs/create-a-new-react-app.html)
- [`styled-components`](https://styled-components.com/)

## License

ISC
